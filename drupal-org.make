; Drupal.org release file.
core = 7.x
api = 2

; Contrib Modules
projects[admin_menu][subdir] = "contrib"
projects[admin_menu][version] = "3.0-rc5"

projects[ctools][subdir] = "contrib"
projects[ctools][version] = "1.12"

projects[date][subdir] = "contrib"
projects[date][version] = "2.10"

projects[diff][subdir] = "contrib"
projects[diff][version] = "3.3"

projects[distro_update][subdir] = "contrib"
projects[distro_update][version] = "1.0-beta4"

projects[entity][subdir] = "contrib"
projects[entity][version] = "1.8"

projects[entity_view_mode][subdir] = "contrib"
projects[entity_view_mode][version] = "1.0-rc1"

projects[features][subdir] = "contrib"
projects[features][version] = "2.10"

projects[field_extrawidgets][subdir] = "contrib"
projects[field_extrawidgets][version] = "1.1"

projects[field_group][subdir] = "contrib"
projects[field_group][version] = "1.5"

projects[file_entity][subdir] = "contrib"
projects[file_entity][version] = "2.3"

projects[imagecache_actions][subdir] = "contrib"
projects[imagecache_actions][version] = "1.7"

projects[libraries][subdir] = "contrib"
projects[libraries][version] = "2.3"

projects[link][subdir] = "contrib"
projects[link][version] = "1.4"

projects[media][subdir] = "contrib"
projects[media][version] = "2.9"

projects[oauth][subdir] = "contrib"
projects[oauth][version] = "3.4"

projects[services][subdir] = "contrib"
projects[services][version] = "3.20"

projects[services_client][subdir] = "contrib"
projects[services_client][version] = "1.0-beta1"
projects[services_client][patch][] = http://drupal.org/files/services_client.1976516.4.patch

projects[setup][subdir] = "contrib"
projects[setup][version] = "1.x-dev"
projects[setup][patch][] = http://drupal.org/files/setup-2016897-1.patch
projects[setup][patch][] = http://drupal.org/files/setup_2020701_1.patch

projects[strongarm][subdir] = "contrib"
projects[strongarm][version] = "2.0"

projects[system_stream_wrapper][subdir] = "contrib"
projects[system_stream_wrapper][version] = "1.0-rc1"

projects[token][subdir] = "contrib"
projects[token][version] = "1.7"

projects[uuid][subdir] = "contrib"
projects[uuid][version] = "1.0"

projects[uuid_features][subdir] = "contrib"
projects[uuid_features][version] = "1.0-rc1"

projects[views][subdir] = "contrib"
projects[views][version] = "3.16"

projects[wysiwyg][subdir] = "contrib"
projects[wysiwyg][version] = "2.4"

; Bassets modules
projects[bassets_client][subdir] = "contrib"
projects[bassets_client][version] = "1.1"

projects[bassets_media][subdir] = "contrib"
projects[bassets_media][version] = "1.0"

projects[bassets_sw][subdir] = "contrib"
projects[bassets_sw][version] = "1.0"

projects[bassets_scc][subdir] = "contrib"
projects[bassets_scc][version] = "1.0"

projects[bassets_setup][subdir] = "contrib"
projects[bassets_setup][version] = "1.0"


; Libraries
libraries[ckeditor][download][type] = "file"
libraries[ckeditor][download][url] = "http://download.cksource.com/CKEditor/CKEditor/CKEditor%203.6.6.2/ckeditor_3.6.6.2.zip"
libraries[ckeditor][directory_name] = "ckeditor"
libraries[ckeditor][type] = "library"

