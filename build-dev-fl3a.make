includes[drupal-org] = drupal-org.make

projects[bassets_client][subdir] = "contrib"
projects[bassets_client][download][type] = git
projects[bassets_client][download][url] = fl3a@git.drupal.org:project/bassets_client.git
projects[bassets_client][type] = "module"
projects[bassets_client][download][branch] = 7.x-1.x

projects[bassets_media][subdir] = "contrib"
projects[bassets_media][download][type] = git
projects[bassets_media][download][url] = fl3a@git.drupal.org:project/bassets_media.git
projects[bassets_media][type] = "module"
projects[bassets_media][download][branch] = 7.x-1.x

projects[bassets_sw][subdir] = "contrib"
projects[bassets_sw][download][type] = git
projects[bassets_sw][download][url] = fl3a@git.drupal.org:project/bassets_sw.git
projects[bassets_sw][type] = "module"
projects[bassets_sw][download][branch] = 7.x-1.x

projects[bassets_scc][subdir] = "contrib"
projects[bassets_scc][download][type] = git
projects[bassets_scc][download][url] = fl3a@git.drupal.org:project/bassets_scc.git
projects[bassets_scc][type] = "module"
projects[bassets_scc][download][branch] = 7.x-1.x

projects[bassets_setup][subdir] = "contrib"
projects[bassets_setup][download][type] = git
projects[bassets_setup][download][url] = fl3a@git.drupal.org:project/bassets_setup.git
projects[bassets_setup][type] = "module"
projects[bassets_setup][download][branch] = 7.x-1.x
