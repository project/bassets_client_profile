<?php

/**
 * @file
 * Performs a standard Drupal installation with additional configuration of the
 * Drupal Bassets modules and components.
 */

/**
 * Implements hook_form_FORM_ID_alter().
 */
function bassets_client_profile_form_install_configure_form_alter(&$form, $form_state) {
  $form['site_information']['site_name']['#default_value'] = 'Bassets client - ' . $_SERVER['SERVER_NAME'];
}

/**
 * Implements hook_install_tasks_alter().
 */
function bassets_client_profile_install_tasks_alter(&$tasks, $install_state) {
  global $install_state;
  $tasks['install_finished']['function'] = 'bassets_client_profile_install_finished';
  // Skip profile selection step.
  $tasks['install_select_profile']['display'] = FALSE;
  // Skip language selection install step and default language to English.
  $tasks['install_select_locale']['display'] = FALSE;
  $tasks['install_select_locale']['run'] = INSTALL_TASK_SKIP;
  $install_state['parameters']['locale'] = 'en';
  if (!isset($tasks['file_system'])) {
    $finish_task = $tasks['install_finished'];
    unset($tasks['install_finished']);
     module_load_include('inc', 'system', 'system.admin');

    $tasks['file_system'] = array(
      'display_name' => st('File system'),
      'type' => 'form',
      'function' => 'bassets_client_profile_install_file_system',
    );
    $tasks = array_merge($tasks, array('install_finished' => $finish_task));
  }
}

/**
 * Replaces the output of install_finished().
 *
 * @see bassets_client_profile_install_tasks_alter().
 *
 */
function bassets_client_profile_install_finished(&$install_state) {
  install_finished($install_state);
  $messages = drupal_set_message();
  $output = '<p>' . st('Congratulations, you installed @drupal!', array('@drupal' => drupal_install_profile_distribution_name())) . '</p>';
  $output .= '<p>' . (isset($messages['error']) ? st('Review the messages above before you connect this site <a href="@url">with a Bassets server</a>.', array('@url' => url('setup'))) : st('<a href="@url">Connect now your client with a Bassets server</a>.', array('@url' => url('setup')))) . '</p>';
  return $output;
}

/**
 * Form for the - file_system - install task.
 */
function bassets_client_profile_install_file_system($form, &$form_state, $install_state) {
  global $install_state;
  drupal_set_title(t('File system'), PASS_THROUGH);
  $page = isset($install_state['parameters']['filesystem_page']) ? $install_state['parameters']['filesystem_page'] : 1;
  $form = system_file_system_settings();
  if ($page == 1) {
    $form['file_private_path']['#default_value'] = $form['file_public_path']['#default_value'] . '/private';
    unset($form['file_default_scheme']);
  }
  else{
    unset($form['file_private_path']);
    unset($form['file_public_path']);
    unset($form['file_temporary_path']);
  }
  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => st('Save and continue'),
    '#weight' => 15,
  );
  $form['#submit'][] = 'bassets_client_profile_install_file_system_submit';
  return $form;
}

/**
 * Submit handler for bassets_client_profile_install_file_system().
 */
function bassets_client_profile_install_file_system_submit(&$form, &$form_state) {
  global $install_state;
  $page = isset($install_state['parameters']['filesystem_page']) ? $install_state['parameters']['filesystem_page'] : 1;

  if ($page == 1) {
    $install_state['parameters']['filesystem_page'] = 2;
    $install_state['parameters_changed'] = TRUE;
    $install_state['task_not_complete'] = TRUE;
  }
}

/**
 * Implements hook_install().
 *
 * Perform actions to set up the site for this profile.
 */
function bassets_client_profile_install() {
// Add text formats.
  $filtered_html_format = array(
    'format' => 'filtered_html',
    'name' => 'Filtered HTML',
    'weight' => 0,
    'filters' => array(
      // URL filter.
      'filter_url' => array(
        'weight' => 0,
        'status' => 1,
      ),
      // HTML filter.
      'filter_html' => array(
        'weight' => 1,
        'status' => 1,
      ),
      // Line break filter.
      'filter_autop' => array(
        'weight' => 2,
        'status' => 1,
      ),
      // HTML corrector filter.
      'filter_htmlcorrector' => array(
        'weight' => 10,
        'status' => 1,
      ),
    ),
  );
  $filtered_html_format = (object) $filtered_html_format;
  filter_format_save($filtered_html_format);

  $full_html_format = array(
    'format' => 'full_html',
    'name' => 'Full HTML',
    'weight' => 1,
    'filters' => array(
      // URL filter.
      'filter_url' => array(
        'weight' => 0,
        'status' => 1,
      ),
      // Line break filter.
      'filter_autop' => array(
        'weight' => 1,
        'status' => 1,
      ),
      // HTML corrector filter.
      'filter_htmlcorrector' => array(
        'weight' => 10,
        'status' => 1,
      ),
    ),
  );
  $full_html_format = (object) $full_html_format;
  filter_format_save($full_html_format);

  // Create credits menu.
  $menu = array(
    'menu_name' => 'credits',
    'title' => 'Credits',
    'description' => 'Credits',
  );
  menu_save($menu);

  // Create a Reinblau link in the credits menu.
  $item = array(
    'link_title' => st('Reinblau'),
    'link_path' => 'http://www.reinblau.de',
    'menu_name' => 'credits',
  );
  menu_link_save($item);


  // menu_rebuild() is called below...

  // Enable some standard blocks.
  $default_theme = variable_get('theme_default', 'bartik');
  $admin_theme = 'seven';
  $blocks = array(
    array(
      'module' => 'menu',
      'delta' => 'credits',
      'theme' => $default_theme,
      'status' => 1,
      'weight' => 0,
      'region' => 'footer',
      'pages' => '',
      'title' => '<none>',
      'cache' => -1,
    ),
    array(
      'module' => 'system',
      'delta' => 'main',
      'theme' => $default_theme,
      'status' => 1,
      'weight' => 0,
      'region' => 'content',
      'pages' => '',
      'title' => '',
      'cache' => -1,
    ),
    array(
      'module' => 'node',
      'delta' => 'recent',
      'theme' => $admin_theme,
      'status' => 1,
      'weight' => 10,
      'region' => 'dashboard_main',
      'pages' => '',
      'title' => '',
      'cache' => -1,
    ),
    array(
      'module' => 'user',
      'delta' => 'login',
      'theme' => $default_theme,
      'status' => 1,
      'weight' => 0,
      'region' => 'sidebar_first',
      'pages' => '',
      'title' => '',
      'cache' => -1,
    ),
    array(
      'module' => 'system',
      'delta' => 'navigation',
      'theme' => $default_theme,
      'status' => 1,
      'weight' => 0,
      'region' => 'sidebar_first',
      'pages' => '',
      'title' => '',
      'cache' => -1,
    ),
    array(
      'module' => 'system',
      'delta' => 'powered-by',
      'theme' => $default_theme,
      'status' => 1,
      'weight' => 10,
      'region' => 'footer',
      'pages' => '',
      'title' => '',
      'cache' => -1,
    ),
    array(
      'module' => 'system',
      'delta' => 'help',
      'theme' => $default_theme,
      'status' => 1,
      'weight' => 0,
      'region' => 'help',
      'pages' => '',
      'title' => '',
      'cache' => -1,
    ),
    array(
      'module' => 'system',
      'delta' => 'main',
      'theme' => $admin_theme,
      'status' => 1,
      'weight' => 0,
      'region' => 'content',
      'pages' => '',
      'title' => '',
      'cache' => -1,
    ),
    array(
      'module' => 'system',
      'delta' => 'help',
      'theme' => $admin_theme,
      'status' => 1,
      'weight' => 0,
      'region' => 'help',
      'pages' => '',
      'title' => '',
      'cache' => -1,
    ),
    array(
      'module' => 'user',
      'delta' => 'login',
      'theme' => $admin_theme,
      'status' => 1,
      'weight' => 10,
      'region' => 'content',
      'pages' => '',
      'title' => '',
      'cache' => -1,
    ),
    array(
      'module' => 'user',
      'delta' => 'new',
      'theme' => $admin_theme,
      'status' => 1,
      'weight' => 0,
      'region' => 'dashboard_sidebar',
      'pages' => '',
      'title' => '',
      'cache' => -1,
    ),
  );
  $query = db_insert('block')->fields(array('module', 'delta', 'theme', 'status', 'weight', 'region', 'pages', 'title', 'cache'));
  foreach ($blocks as $block) {
    $query->values($block);
  }
  $query->execute();

  // Insert default pre-defined node types into the database. For a complete
  // list of available node type attributes, refer to the node type API
  // documentation at: http://api.drupal.org/api/HEAD/function/hook_node_info.
  $types = array(
    array(
      'type' => 'page',
      'name' => st('Basic page'),
      'base' => 'node_content',
      'description' => st("Use <em>basic pages</em> for your static content, such as an 'About us' page."),
      'custom' => 1,
      'modified' => 1,
      'locked' => 0,
    ),
    array(
      'type' => 'article',
      'name' => st('Article'),
      'base' => 'node_content',
      'description' => st('Use <em>articles</em> for time-sensitive content like news, press releases or blog posts.'),
      'custom' => 1,
      'modified' => 1,
      'locked' => 0,
    ),
  );

  foreach ($types as $type) {
    $type = node_type_set_defaults($type);
    node_type_save($type);
    node_add_body_field($type);
  }

  // Insert default pre-defined RDF mapping into the database.
  $rdf_mappings = array(
    array(
      'type' => 'node',
      'bundle' => 'page',
      'mapping' => array(
        'rdftype' => array('foaf:Document'),
      ),
    ),
    array(
      'type' => 'node',
      'bundle' => 'article',
      'mapping' => array(
        'field_image' => array(
          'predicates' => array('og:image', 'rdfs:seeAlso'),
          'type' => 'rel',
        ),
        'field_tags' => array(
          'predicates' => array('dc:subject'),
          'type' => 'rel',
        ),
      ),
    ),
  );
  foreach ($rdf_mappings as $rdf_mapping) {
    rdf_mapping_save($rdf_mapping);
  }

  // Default "Basic page" to not be promoted and have comments disabled.
  variable_set('node_options_page', array('status'));
  variable_set('comment_page', COMMENT_NODE_HIDDEN);

  // Don't display date and author information for "Basic page" nodes by default.
  variable_set('node_submitted_page', FALSE);

  // Enable user picture support and set the default to a square thumbnail option.
  variable_set('user_pictures', '1');
  variable_set('user_picture_dimensions', '1024x1024');
  variable_set('user_picture_file_size', '800');
  variable_set('user_picture_style', 'thumbnail');

  // Allow visitor account creation with administrative approval.
  variable_set('user_register', USER_REGISTER_VISITORS_ADMINISTRATIVE_APPROVAL);

  // Create a default vocabulary named "Tags", enabled for the 'article' content type.
  $description = st('Use tags to group articles on similar topics into categories.');
  $help = st('Enter a comma-separated list of words to describe your content.');
  $vocabulary = (object) array(
    'name' => st('Tags'),
    'description' => $description,
    'machine_name' => 'tags',
    'help' => $help,

  );
  taxonomy_vocabulary_save($vocabulary);

  $field = array(
    'field_name' => 'field_' . $vocabulary->machine_name,
    'type' => 'taxonomy_term_reference',
    // Set cardinality to unlimited for tagging.
    'cardinality' => FIELD_CARDINALITY_UNLIMITED,
    'settings' => array(
      'allowed_values' => array(
        array(
          'vocabulary' => $vocabulary->machine_name,
          'parent' => 0,
        ),
      ),
    ),
  );
  field_create_field($field);

  $instance = array(
    'field_name' => 'field_' . $vocabulary->machine_name,
    'entity_type' => 'node',
    'label' => 'Tags',
    'bundle' => 'article',
    'description' => $vocabulary->help,
    'widget' => array(
      'type' => 'taxonomy_autocomplete',
      'weight' => -4,
    ),
    'display' => array(
      'default' => array(
        'type' => 'taxonomy_term_reference_link',
        'weight' => 10,
      ),
      'teaser' => array(
        'type' => 'taxonomy_term_reference_link',
        'weight' => 10,
      ),
    ),
  );
  field_create_instance($instance);


  // Create an image field named "Image", enabled for the 'article' content type.
  // Many of the following values will be defaulted, they're included here as an illustrative examples.
  // See http://api.drupal.org/api/function/field_create_field/7

  $field = array(
    'field_name' => 'field_image',
    'type' => 'image',
    'cardinality' => 1,
    'locked' => FALSE,
    'indexes' => array('fid' => array('fid')),
    'settings' => array(
      'uri_scheme' => 'public',
      'default_image' => FALSE,
    ),
    'storage' => array(
      'type' => 'field_sql_storage',
      'settings' => array(),
    ),
  );
  field_create_field($field);


  // Many of the following values will be defaulted, they're included here as an illustrative examples.
  // See http://api.drupal.org/api/function/field_create_instance/7
  $instance = array(
    'field_name' => 'field_image',
    'entity_type' => 'node',
    'label' => 'Image',
    'bundle' => 'article',
    'description' => st('Upload an image to go with this article.'),
    'required' => FALSE,

    'settings' => array(
      'file_directory' => 'field/image',
      'file_extensions' => 'png gif jpg jpeg',
      'max_filesize' => '',
      'max_resolution' => '',
      'min_resolution' => '',
      'alt_field' => TRUE,
      'title_field' => '',
    ),

    'widget' => array(
      'type' => 'image_image',
      'settings' => array(
        'progress_indicator' => 'throbber',
        'preview_image_style' => 'thumbnail',
      ),
      'weight' => -1,
    ),

    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'type' => 'image',
        'settings' => array('image_style' => 'large', 'image_link' => ''),
        'weight' => -1,
      ),
      'teaser' => array(
        'label' => 'hidden',
        'type' => 'image',
        'settings' => array('image_style' => 'medium', 'image_link' => 'content'),
        'weight' => -1,
      ),
    ),
  );
  field_create_instance($instance);

  // Enable default permissions for system roles.
  $filtered_html_permission = filter_permission_name($filtered_html_format);
  user_role_grant_permissions(DRUPAL_ANONYMOUS_RID, array('access content', 'access comments', $filtered_html_permission));
  user_role_grant_permissions(DRUPAL_AUTHENTICATED_RID, array('access content', 'access comments', 'post comments', 'skip comment approval', $filtered_html_permission));

  // Create a default role for site administrators, with all available permissions assigned.
  $admin_role = new stdClass();
  $admin_role->name = 'administrator';
  $admin_role->weight = 2;
  user_role_save($admin_role);
  user_role_grant_permissions($admin_role->rid, array_keys(module_invoke_all('permission')));
  // Set this as the administrator role.
  variable_set('user_admin_role', $admin_role->rid);

  // Assign user 1 the "administrator" role.
  db_insert('users_roles')
    ->fields(array('uid' => 1, 'rid' => $admin_role->rid))
    ->execute();

  // Create a Home link in the main menu.
  $item = array(
    'link_title' => st('Home'),
    'link_path' => '<front>',
    'menu_name' => 'main-menu',
  );
  menu_link_save($item);

  // Update the menu router information.
  menu_rebuild();

  // Enable the admin theme.
  db_update('system')
    ->fields(array('status' => 1))
    ->condition('type', 'theme')
    ->condition('name', 'seven')
    ->execute();
  variable_set('admin_theme', 'seven');
  variable_set('node_admin_theme', '1');

  // Set Bassets logo and favicon.
  $bartik = variable_get('theme_bartik_settings', array());
  $bartik['toggle_logo'] = 1;
  $bartik['default_logo'] = 0;
  $bartik['logo_path'] = 'profiles/bassets_client_profile/img/bassets-logo.png';
  $bartik['toggle_favicon'] = 1;
  $bartik['default_favicon'] = 0;
  $bartik['favicon_path'] = 'profiles/bassets_client_profile/img/favicon.ico';
  $bartik['theme'] = 'bartik';
  variable_set('theme_bartik_settings', $bartik);
}

/**
 * Deletes the fields:
 * field_image_tags
 *
 * Prerelease Update.
 */
function bassets_client_profile_update_7001() {
  $fields = array('field_image_tags');
  foreach ($fields as $field) {
    field_delete_field($field);
  }
}

/**
 * Deletes the fields:
 * field_rr_license,
 * field_rr_license_url,
 * field_license_type,
 * field_license
 *
 * Deletes the vocabulary:
 * license
 *
 * Prerelease Update.
 */
function bassets_client_profile_update_7002() {
  $fields = array('field_rr_license', 'field_rr_license_url', 'field_license_type', 'field_license');
  foreach ($fields as $field) {
    field_delete_field($field);
  }
  $voc = taxonomy_vocabulary_machine_name_load('license');
  if (!empty($voc)) {
    taxonomy_vocabulary_delete($voc->vid);
  }
}

/**
 * - Reverts bassets_client_demo.
 * - Enables bassets_client_entity_view_mode.
 * - Fills the new field with the data of the old field
 * - Deletes the field field_bassets_file.
 *
 * Prerelease Update.
 */
function bassets_client_profile_update_7003() {
  $revert = array('bassets_client_demo' => array('field_base', 'field_instance'));
  features_revert($revert);
  module_enable(array('bassets_client_entity_view_mode'));
  $revert = array('bassets_client_entity_view_mode' => array('file_display'));
  features_revert($revert);
  $query = new EntityFieldQuery;
  $result = $query
    ->entityCondition('entity_type', 'node')
    ->entityCondition('bundle', 'bassets_demo')
    ->fieldCondition('field_bassets_file', 'fid', 'NULL', '!=')
    ->execute();
  if (!empty($result['node'])) {
    $entities = entity_load('node', array_keys($result['node']));
    foreach ($entities as $entity) {
      $entity_wrapped = entity_metadata_wrapper('node', $entity);
      $current_value = $entity_wrapped->field_bassets_file->value();
      if (!empty($current_value)) {
        $entity_wrapped->field_bassets_image->set($current_value);
        $entity_wrapped->save();
      }
    }
  }
  $fields = array('field_bassets_file');
  foreach ($fields as $field) {
    field_delete_field($field);
  }
}

/**
 * Enable distro_update module.
 */
function bassets_client_profile_update_7004() {
  module_enable(array('distro_update'));
}
